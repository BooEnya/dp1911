package com.automation.testing;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDBClient extends BaseDBClient {

    private static UserDBClient userDBClient;
    private String host;
    private String user;
    private String password;

    private UserDBClient(String host, String user, String password) {
        this.host = host;
        this.user = user;
        this.password = password;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Failed to initialize mysql driver", e);
        }
    }

    public void createTableIfNotExists() {
        try (Connection connection = DriverManager.getConnection(host, user, password)) {
            Statement statement = connection.createStatement();
            statement.execute("create table if not exists user(" +
                    "id integer primary key auto_increment, " +
                    "name varchar(100));");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create [user] table", e);
        }
    }

    public static UserDBClient getInstant(String host, String user, String password) {
        if (null == userDBClient) {
             userDBClient = new UserDBClient(host, user, password);
        }
        return userDBClient;
    }


}
