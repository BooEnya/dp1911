package com.automation.testing.client.rest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public abstract class BaseRestClient {

    static {
        RestAssured.useRelaxedHTTPSValidation();
    }

    protected final RequestSpecification requestSpecification;

    public BaseRestClient(String url, ContentType contentType) {
        requestSpecification = RestAssured.given()
                .baseUri(url)
                .contentType(contentType)
                .log().ifValidationFails();
    }

}
