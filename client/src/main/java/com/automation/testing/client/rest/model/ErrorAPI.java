package com.automation.testing.client.rest.model;

public class ErrorAPI {

    private String message;

    public String getMessage() {
        return message;
    }

    public ErrorAPI setMessage(String message) {
        this.message = message;
        return this;
    }
}
