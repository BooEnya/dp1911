package com.automation.testing.client.soap;

import com.automation.testing.build.ping.PingService;
import com.automation.testing.build.ping.PingServicePortType;

import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;

public class PingSoapClient {

    private PingService pingService;

    public PingSoapClient() {
        try {
            pingService = new PingService(new URL(""));

            ((BindingProvider) pingService).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "user");
            ((BindingProvider) pingService).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "password");
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failed to connect to PingService", e);
        }
    }

    public PingServicePortType getPingServicePort() {
        return pingService.getPingServicePort();
    }
}
