package com.automation.testing.client.rest;

import com.automation.testing.client.rest.model.UserDTO;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class UserRestClient extends BaseRestClient {

    public UserRestClient(String url, ContentType contentType) {
        super(url, contentType);
    }

    public UserDTO queryUserByName(String name) {
        return queryUserByName(name, 200);
    }

    public UserDTO queryUserByName(String name, int expectedStatusCode) {
        return queryUserByName(name, expectedStatusCode, UserDTO.class);
    }

    public <T> T queryUserByName(String name, int expectedStatusCode, Class<T> tClass) {
        Response response = RestAssured
                .given()
                .spec(requestSpecification)
                .queryParams("name", name)
                .get("/user");

        response.then().statusCode(expectedStatusCode);

        return response.as(tClass);
    }

    public UserDTO createUser(UserDTO userDTO) {
        return createUser(userDTO, 201);
    }

    public UserDTO createUser(UserDTO userDTO, int expectedStatusCode) {
        return createUser(userDTO, expectedStatusCode, UserDTO.class);
    }

    public <T> T createUser(UserDTO userDTO, int expectedStatusCode, Class<T> tClass) {
        Response response = RestAssured
                .given()
                .spec(requestSpecification)
                .body(userDTO)
                .post("/user");

        response.then().statusCode(expectedStatusCode);

        return response.as(tClass);
    }
}
