package com.automation.testing.client.rest.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"id", "name"})
public class UserDTO {

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    @XmlElement
    public UserDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public UserDTO setName(String name) {
        this.name = name;
        return this;
    }
}
