package com.automation.testing.scripts.commons;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class GeneralTestngListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println(String.format("[%s] Test has been started", iTestResult.getName()));
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println(String.format("[%s] Test has been finished", iTestResult.getName()));
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println(String.format("[%s] Test has been failed", iTestResult.getName()));
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
