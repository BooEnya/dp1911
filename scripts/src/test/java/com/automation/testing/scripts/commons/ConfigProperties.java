package com.automation.testing.scripts.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigProperties {

    private static final String PROPERTIES_PATH = "config.properties";
    private static final Properties properties = new Properties();

    static {
        try {
            properties.load(new FileInputStream(new File(PROPERTIES_PATH)));
        } catch (IOException e) {
            throw new RuntimeException("Failed to read config properties file: " + PROPERTIES_PATH, e);
        }
    }

    private ConfigProperties() {
    }

    public static String getApplicationHost() {
        return getProperty("app.host");
    }

    public static String getApplicationUser() {
        return getProperty("app.user");
    }

    public static String getApplicationPassword() {
        return getProperty("app.password");
    }

    private static String getProperty(String propertyName) {
        String systemProperty = System.getProperty(propertyName);
        return null == systemProperty ? properties.getProperty(propertyName) : systemProperty;
    }
}
