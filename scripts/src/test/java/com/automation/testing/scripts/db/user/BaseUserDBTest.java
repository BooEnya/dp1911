package com.automation.testing.scripts.db.user;

import com.automation.testing.UserDBClient;
import com.automation.testing.scripts.db.BaseDBTest;
import org.testng.annotations.BeforeClass;

public class BaseUserDBTest extends BaseDBTest {

    protected UserDBClient userDBClient;

    @BeforeClass(alwaysRun = true)
    public void setUpDBUserTableClass() {
        userDBClient = UserDBClient.getInstant(mySQLContainer.getJdbcUrl(), mySQLContainer.getUsername(), mySQLContainer.getPassword());
        System.out.println(userDBClient.toString());
        System.out.println(UserDBClient.getInstant(mySQLContainer.getJdbcUrl(), mySQLContainer.getUsername(), mySQLContainer.getPassword()).toString());
        userDBClient.createTableIfNotExists();
    }


}
