package com.automation.testing.scripts.rest.user.json;

import com.automation.testing.client.rest.model.UserDTO;
import com.automation.testing.scripts.commons.TestngGroups;
import org.testng.annotations.Test;

public class TestCreateSimpleUserJson extends BaseUserRestJsonTest {

    @Test(groups = TestngGroups.REST)
    public void testCreateSimpleUserJson() {
        UserDTO actualCreatedUser = userRestClient.createUser(requestedUser);

        verifyUserDTO(actualCreatedUser, expectedCreatedUser, "Failed verification of created user (JSON)");
    }
}
