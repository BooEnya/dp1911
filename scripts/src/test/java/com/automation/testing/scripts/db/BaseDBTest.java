package com.automation.testing.scripts.db;

import org.testcontainers.containers.MySQLContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public abstract class BaseDBTest {

    protected MySQLContainer mySQLContainer;

    @BeforeSuite
    public final void setUpBaseDBSuite() {
        mySQLContainer = new MySQLContainer();
    }


    @BeforeClass(alwaysRun = true)
    public final void setUpBaseDBClass() {
        mySQLContainer.start();
    }

    @AfterClass(alwaysRun = true)
    public final void tearDownBaseDBClass() {
        if (null != mySQLContainer) {
            mySQLContainer.stop();
        }
    }
}
