package com.automation.testing.scripts.rest.user.json;

import com.automation.testing.client.rest.model.UserDTO;
import com.automation.testing.scripts.commons.TestngGroups;
import org.testng.annotations.Test;

public class TestFindSimpleUserJson extends BaseUserRestJsonTest {

    @Test(groups = TestngGroups.REST)
    public void testFindSimpleUserJson() {
        UserDTO actualFoundUser = userRestClient.queryUserByName(queryParams.values().iterator().next().get(0));

        verifyUserDTO(actualFoundUser, expectedFoundUser, "Failed verification of found user (JSON)");
    }
}
