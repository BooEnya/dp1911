package com.automation.testing.scripts.commons;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Serialization {

    public static void main(String[] args) {
        String filename = "scripts/target/users.dat";
        List<User> users = new ArrayList<>();
        users.add(new User().setName("Bob"));
        users.add(new User().setName("Peter"));

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(new File(filename)))) {
            objectOutputStream.writeObject(users);
        } catch (IOException e) {
            throw new RuntimeException("Failed to serialize users", e);
        }

        List<User> parsedUsers = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            parsedUsers = (List<User>) ois.readObject();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        for (User user : parsedUsers) {
            System.out.println("User name: " + user.getName());
        }
    }

    private static class User implements Serializable {

        private static final long serialVersionUID = 112343141L;

        private String name;

        public String getName() {
            return name;
        }

        public User setName(String name) {
            this.name = name;
            return this;
        }
    }
}
