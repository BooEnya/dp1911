package com.automation.testing.scripts.rest;

import com.automation.testing.client.rest.model.UserDTO;
import org.mockserver.model.Header;
import org.testcontainers.containers.MockServerContainer;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public abstract class BaseRestTest {

    protected static final Header CONTENT_TYPE_JSON_HEADER = new Header("Content-Type", "application/json");
    protected static final Header CONTENT_TYPE_XML_HEADER = new Header("Content-Type", "application/xml");
    protected static MockServerContainer mockServerContainer = new MockServerContainer("5.5.4");

    @BeforeSuite(alwaysRun = true)
    public final void setUpRestClientSuite() {
        mockServerContainer.start();
    }

    @AfterSuite(alwaysRun = true)
    public final void tearDownRestClientSuite() {
        mockServerContainer.stop();
    }


    protected String objectToXmlString(Object object) {
        try {
            StringWriter stringWriter = new StringWriter();

            JAXBContext jaxbContext = JAXBContext.newInstance(UserDTO.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(object, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            throw new RuntimeException("Failed to convert object", e);
        }
    }
}
