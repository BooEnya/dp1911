package com.automation.testing.scripts.rest.user;

import com.automation.testing.client.rest.UserRestClient;
import com.automation.testing.client.rest.model.ErrorAPI;
import com.automation.testing.client.rest.model.UserDTO;
import com.automation.testing.scripts.rest.BaseRestTest;
import io.restassured.http.ContentType;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.BodyWithContentType;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class BaseUserRestTest extends BaseRestTest {

    private static final String PATH = "/user";
    protected UserRestClient userRestClient;
    protected UserDTO requestedUser = new UserDTO().setName("Bob");
    protected UserDTO expectedCreatedUser = new UserDTO().setId(1).setName("CreatedBob");
    protected UserDTO expectedFoundUser = new UserDTO().setId(2).setName("FoundPeter");
    protected Map<String, List<String>> queryParams = Collections.singletonMap("name", Collections.singletonList("Peter"));
    private ErrorAPI existingUserError = new ErrorAPI().setMessage("User with specified name has already been created");


    @BeforeSuite(alwaysRun = true)
    public void setUpUserRestSuite() {
        // POST requests
        new MockServerClient(mockServerContainer.getContainerIpAddress(), mockServerContainer.getServerPort())
                .when(HttpRequest.request()
                        .withMethod("POST")
                        .withPath(PATH)
                        .withBody(convertDTOToMockBody(requestedUser)))
                .respond(HttpResponse.response()
                        .withStatusCode(201)
                        .withHeaders(getMockContentType())
                        .withBody(convertDTOToMockBody(expectedCreatedUser)));

        new MockServerClient(mockServerContainer.getContainerIpAddress(), mockServerContainer.getServerPort())
                .when(HttpRequest.request()
                        .withMethod("POST")
                        .withPath(PATH))
                .respond(HttpResponse.response()
                        .withStatusCode(400)
                        .withHeaders(getMockContentType())
                        .withBody(convertDTOToMockBody(existingUserError)));

        // GET requests
        new MockServerClient(mockServerContainer.getContainerIpAddress(), mockServerContainer.getServerPort())
                .when(HttpRequest.request()
                        .withMethod("GET")
                        .withPath(PATH)
                        .withQueryStringParameters(queryParams))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withHeaders(getMockContentType())
                        .withBody(convertDTOToMockBody(expectedFoundUser)));
    }

    @BeforeClass(alwaysRun = true)
    public void setUpUserRestClass() {
        userRestClient = new UserRestClient(mockServerContainer.getEndpoint(), getClientContentType());
    }

    protected abstract BodyWithContentType convertDTOToMockBody(Object dto);

    protected abstract ContentType getClientContentType();

    protected abstract Header getMockContentType();

    protected void verifyUserDTO(UserDTO actualUser, UserDTO expectedUser, String errorMessage) {
        Assert.assertEquals(actualUser.getId(), expectedUser.getId(), "[" + errorMessage + "]. Unexpected id in created user");
        Assert.assertEquals(actualUser.getName(), expectedUser.getName(), "[" + errorMessage + "]. Unexpected id in created user");
    }
}
