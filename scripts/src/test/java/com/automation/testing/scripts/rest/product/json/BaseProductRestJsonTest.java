package com.automation.testing.scripts.rest.product.json;

import com.automation.testing.scripts.rest.user.BaseUserRestTest;
import io.restassured.http.ContentType;
import org.mockserver.model.BodyWithContentType;
import org.mockserver.model.Header;
import org.mockserver.model.JsonBody;

public abstract class BaseProductRestJsonTest extends BaseUserRestTest {

    @Override
    protected BodyWithContentType convertDTOToMockBody(Object dto) {
        return JsonBody.json(dto);
    }

    @Override
    protected ContentType getClientContentType() {
        return ContentType.JSON;
    }

    @Override
    protected Header getMockContentType() {
        return CONTENT_TYPE_JSON_HEADER;
    }
}
