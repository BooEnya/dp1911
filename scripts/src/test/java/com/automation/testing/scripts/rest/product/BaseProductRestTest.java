package com.automation.testing.scripts.rest.product;

import com.automation.testing.client.rest.model.ErrorAPI;
import com.automation.testing.client.rest.model.ProductDTO;
import com.automation.testing.scripts.rest.BaseRestTest;
import io.restassured.http.ContentType;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.BodyWithContentType;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.testng.annotations.BeforeSuite;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class BaseProductRestTest extends BaseRestTest {

    private static final String PATH = "/product";
    private ProductDTO requestedProduct = new ProductDTO().setName("Subscription");
    private ProductDTO expectedCreatedProduct = new ProductDTO().setId(1).setName("CreatedSubscription");
    private ProductDTO expectedFoundProduct = new ProductDTO().setId(2).setName("FoundSubscription");
    private Map<String, List<String>> queryParams = Collections.singletonMap("name", Collections.singletonList("CreatedSubscription"));
    private ErrorAPI existingProductError = new ErrorAPI().setMessage("Product with specified name has already been created");


    @BeforeSuite(alwaysRun = true)
    public void setUpUserRestSuite() {
        // POST requests
        new MockServerClient(mockServerContainer.getContainerIpAddress(), mockServerContainer.getServerPort())
                .when(HttpRequest.request()
                        .withMethod("POST")
                        .withPath(PATH)
                        .withBody(convertDTOToMockBody(requestedProduct)))
                .respond(HttpResponse.response()
                        .withStatusCode(201)
                        .withHeaders(getMockContentType())
                        .withBody(convertDTOToMockBody(expectedCreatedProduct)));

        new MockServerClient(mockServerContainer.getContainerIpAddress(), mockServerContainer.getServerPort())
                .when(HttpRequest.request()
                        .withMethod("POST")
                        .withPath(PATH))
                .respond(HttpResponse.response()
                        .withStatusCode(400)
                        .withHeaders(getMockContentType())
                        .withBody(convertDTOToMockBody(existingProductError)));

        // GET requests
        new MockServerClient(mockServerContainer.getContainerIpAddress(), mockServerContainer.getServerPort())
                .when(HttpRequest.request()
                        .withMethod("GET")
                        .withPath(PATH)
                        .withQueryStringParameters(queryParams))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withHeaders(getMockContentType())
                        .withBody(convertDTOToMockBody(expectedFoundProduct)));
    }

    protected abstract BodyWithContentType convertDTOToMockBody(Object dto);

    protected abstract ContentType getClientContentType();

    protected abstract Header getMockContentType();
}
