package com.automation.testing.scripts.rest.user.xml;

import com.automation.testing.scripts.rest.user.BaseUserRestTest;
import io.restassured.http.ContentType;
import org.mockserver.model.BodyWithContentType;
import org.mockserver.model.Header;
import org.mockserver.model.XmlBody;

public abstract class BaseUserRestXMLTest extends BaseUserRestTest {

    @Override
    protected BodyWithContentType convertDTOToMockBody(Object object) {
        return XmlBody.xml(objectToXmlString(object));
    }

    @Override
    protected ContentType getClientContentType() {
        return ContentType.XML;
    }

    @Override
    protected Header getMockContentType() {
        return CONTENT_TYPE_XML_HEADER;
    }
}
