package com.automation.testing.scripts.rest.user.xml;

import com.automation.testing.client.rest.model.UserDTO;
import com.automation.testing.scripts.commons.TestngGroups;
import com.automation.testing.scripts.rest.user.json.BaseUserRestJsonTest;
import org.testng.annotations.Test;

public class TestCreateSimpleUserXML extends BaseUserRestJsonTest {

    @Test(groups = TestngGroups.REST)
    public void testCreateSimpleUserXML() {
        UserDTO actualCreatedUser = userRestClient.createUser(requestedUser);

        verifyUserDTO(actualCreatedUser, expectedCreatedUser, "Failed verification of created user (XML)");
    }
}
