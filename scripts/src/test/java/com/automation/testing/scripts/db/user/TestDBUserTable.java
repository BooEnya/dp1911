package com.automation.testing.scripts.db.user;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class TestDBUserTable extends BaseUserDBTest {

    @BeforeClass(alwaysRun = true)
    public void setUpTestDB1Class() {
        try (Connection connection = DriverManager.getConnection(mySQLContainer.getJdbcUrl(), mySQLContainer.getUsername(), mySQLContainer.getPassword())) {
            List<String> names = Arrays.asList("Bob", "Jack", "Bob");
            try {
                connection.setAutoCommit(false);
                PreparedStatement preparedStatement = connection.prepareStatement("insert into user (name) values (?)");
                names.forEach(
                        name -> {
                            try {
                                preparedStatement.setString(1, name);
                                preparedStatement.execute();
                            } catch (SQLException e) {
                                throw new RuntimeException("Failed to create a new user: " + name, e);
                            }
                        }
                );

                connection.createStatement().execute("insert into users (name) values ('Bob')");

                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot connect to the database", e);
        }
    }

    @Test(groups = {"DB", "testDBUserTable"})
    public void testDBUserTable() {
        try (Connection connection = DriverManager.getConnection(mySQLContainer.getJdbcUrl(), mySQLContainer.getUsername(), mySQLContainer.getPassword())) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user");
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString("name");

                System.out.println(String.format("id: %s; name: %s", id, name));
            }
            System.out.println();
        } catch (SQLException e) {
            throw new RuntimeException("Failed to select all users", e);
        }
        System.out.println();

        try (Connection connection = DriverManager.getConnection(mySQLContainer.getJdbcUrl(), mySQLContainer.getUsername(), mySQLContainer.getPassword())) {
            Statement statement = connection.createStatement();
            statement.execute("CREATE PROCEDURE convertId(p_ID     Integer,\n" +
                    "                                   out       v_ID        Varchar(50))\n" +
                    "BEGIN\n" +
                    " set v_ID  = concat( 'E' , Cast(p_ID as char(15)) );\n" +
                    "END");

            CallableStatement callableStatement = connection.prepareCall("{call convertId(?,?)}");
            callableStatement.setInt(1, 10);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.execute();

            String convertedId = callableStatement.getString(2);
            System.out.println(convertedId);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to select all users", e);
        }
    }

}
