package com.automation.testing.scripts.rest.user.xml;

import com.automation.testing.client.rest.model.UserDTO;
import com.automation.testing.scripts.commons.TestngGroups;
import com.automation.testing.scripts.rest.user.json.BaseUserRestJsonTest;
import org.testng.annotations.Test;

public class TestFindSimpleUserXML extends BaseUserRestJsonTest {

    @Test(groups = TestngGroups.REST)
    public void testFindSimpleUserXML() {
        UserDTO actualFoundUser = userRestClient.queryUserByName(queryParams.values().iterator().next().get(0));

        verifyUserDTO(actualFoundUser, expectedFoundUser, "Failed verification of found user (XML)");
    }
}
